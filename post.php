<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

ini_set("upload_max_filesize", "900M");
ini_set("post_max_size", "910M");
ini_set("upload_tmp_dir", "/tmp");

require 'function.inc.php';
if (@$_GET["new"]) {
  clearPostForm();

}

if (isset($_POST["edit"]) && isset($_SESSION["post_id"])) {
  $commentaire = trim($_POST['comment']);
  updateComment($_SESSION["post_id"], $commentaire);
  if ((!empty($_FILES['mediaI']['tmp_name'][0])) ||
      (!empty($_FILES['mediaV']['tmp_name'][0])) ||
      (!empty($_FILES['mediaA']['tmp_name'][0]))) {
        uploadMedia($_FILES, $_SESSION["post_id"]);
  }

  header('Location: index.php');
}

if (isset($_POST["submit"]) && $_POST["submit"]) {
    EDatabase::beginTransaction();
    $commentaire = trim($_POST['comment']);

    if ($commentaire == "") {
        $_SESSION['message'] = "commentaire obligatoire !!";
        EDatabase::rollBack();
        header('Location: '.$_SERVER['PHP_SELF']);
        die;
    }

    $idPost = insertPost($commentaire, $_POST['mediaType']);
    debug($idPost);
    debug($_POST['mediaType']);

    if ($idPost === false) {
        EDatabase::rollBack();
        return false;
    }



    if ((($_POST['mediaType'] === 'Image') and (!empty($_FILES['mediaI']['tmp_name'][0]))) ||
        (($_POST['mediaType'] === 'Video') and (!empty($_FILES['mediaV']['tmp_name'][0]))) ||
        (($_POST['mediaType'] === 'Audio') and (!empty($_FILES['mediaA']['tmp_name'][0])))) {

          uploadMedia($_FILES, $idPost);
    }
    EDatabase::commit();

    header('Location: index.php');
}

if (isset($_GET["media_id"])) {
  deleteMedia($_GET["media_id"]);
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>HOME</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
  <?php include 'navbar.php'; ?>
  <div class="container">
     <?= flashMessage() ?>

    <form class="mt-4" name="frm_upload" id="frm_upload" action="post.php" method="post" enctype="multipart/form-data">
      <input type="hidden" name="MAX_FILE_SIZE" value="25000000000" />
      <div class='form-group'>
        <label for='comment'>commentaire</label>
        <textarea class='form-control' name='comment' id='comment' rows='3'><?php
          if (isset($_SESSION["post_id"])) {
            showCommentToEdit($_SESSION["post_id"]);
          }
        ?></textarea>
      </div>
      <?php
        radioBtn();
      ?>

      <?php
      if (isset($_SESSION["post_id"])) {
          showMediaToEdit($_SESSION["post_id"]);
      } ?>
      <br>
      <?php if (isset($_SESSION["post_id"])) { ?>
        <input type="submit" class="btn btn-primary" name="edit" value="Edit">
      <?php } else { ?>
        <input type="submit" class="btn btn-primary" name="submit" value="Submit">
      <?php } ?>
    </form>
  </div>
  <?php printFooterScripts(); ?>

</body>
</html>
