<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
require_once './db/database.php';

/**
 * Get all the post with media
 *
 * @return void
 */
function getPostMedia($search = NULL)
{
  if ($search) {
    $sql= "SELECT idmedia, IDPosts, nomMedia, typeMedia, commentaire, datePost, postType from posts left join media on posts.IDPosts = media.posts_IDPosts WHERE datePost LIKE :date order by IDPosts desc";
    $stmt = EDatabase::prepare($sql);
    $stmt->execute(array(':date' => $search));
  }else {
    $sql= "SELECT idmedia, IDPosts, nomMedia, typeMedia, commentaire, datePost, postType from posts left join media on posts.IDPosts = media.posts_IDPosts order by IDPosts desc";
    $stmt = EDatabase::prepare($sql);
    $stmt->execute();
  }

  $results = $stmt->fetchAll(PDO::FETCH_OBJ);

  return parsePostMedia($results);
  // return parsePostMedia($results);
}

/**
 * Get a specified post for an id
 *
 * @param int $id
 * @return parsePostMedia($results)
 */
function getPostMediaWhereId($id)
{
    $sql= "SELECT idmedia, IDPosts, nomMedia, typeMedia, commentaire, datePost from posts inner join media on posts.IDPosts = media.posts_IDPosts WHERE IDPosts = :id order by IDPosts desc";
    $stmt = EDatabase::prepare($sql);
    $stmt->execute(array(':id' => $id));
    $results = $stmt->fetchAll(PDO::FETCH_OBJ);

    return $results;
    // return parsePostMedia($results);
}

function getCommentWhereId($id)
{
    $sql= "SELECT IDPosts, commentaire FROM posts WHERE IDPosts = :id";
    $stmt = EDatabase::prepare($sql);
    $stmt->execute(array(':id' => $id));
    $results = $stmt->fetch(PDO::FETCH_OBJ);

    return $results;
    // return parsePostMedia($results);
}

/**
 * Parse all images and bind it to the right post
 *
 * @param [array] $ary
 * @return array
 */
function parsePostMedia($ary)
{
    $posts = [];
    foreach ($ary as $el) {
        // Ensure that $posts['id'] is an array
        if (!isset($posts[$el->IDPosts])) {
            $posts[$el->IDPosts] = [];
        }
        // define the "id" attribute
        $posts[$el->IDPosts]['id']= $el->IDPosts;
        $posts[$el->IDPosts]['commentaire']= $el->commentaire;
        $posts[$el->IDPosts]['datePost']= $el->datePost;
        $posts[$el->IDPosts]['postType']= $el->postType;

        // Ensure that $posts['id']['medias'] is an array
        if (!isset($posts[$el->IDPosts]['medias'])) {
            $posts[$el->IDPosts]['medias'] = [];
        }
        //$posts[$el->IDPosts]['media'] = [];
        $posts[$el->IDPosts]['medias'][] = array($el->idmedia, $el->typeMedia, $el->nomMedia);
    }
    return $posts;
}

/**
 * Function to insert a media
 *
 * @param string $image
 * @param string $typeMedia
 * @param int $idPost
 * @return void
 */
function insertMedia($image, $typeMedia, $idPost)
{
    $sql = " INSERT INTO media (nomMedia, typeMedia, posts_IDPosts) VALUES (:i, :tm, :idp)";

    try {
        $sth = EDatabase::prepare($sql);
        $sth->execute(array(
            ':i' => $image,
            ':tm' => $typeMedia,
            ':idp' => $idPost
        ));
    } catch (PDOException $e) {
        echo 'Problème de lecture de la base de données: ' . $e->getMessage();
        return false;
    }
    // Done
    return true;
}

/**
 * Function to both move media to the uploaded media into "/uploads" and insert media into database
 *
 * @param array $files
 * @param int $idPost
 * @return bool
 */
function uploadMedia($files, $idPost)
{
    if ($_POST['mediaType'] === 'Image') {
        $mediaSuffix = 'I';
    } elseif ($_POST['mediaType'] === 'Video') {
        $mediaSuffix = 'V';
    } elseif ($_POST['mediaType'] === 'Audio') {
        $mediaSuffix = 'A';
    }
    // Check if there is no upload errors:
    if ($_FILES['media' . $mediaSuffix]['error']) {
        foreach ($_FILES['media' . $mediaSuffix]['error'] as $error) {
            if ($error != 0) {
                $err_msg = codeToMessage($error);
                error_log("Error uploading the file: " . $err_msg);
            }
        }
    }

    foreach ($files['media' . $mediaSuffix]['tmp_name'] as $key => $val) {
        // File upload path
        $media = $files['media' . $mediaSuffix]['name'][$key];
        $typeMedia = $files['media' . $mediaSuffix]['type'][$key];
        $mediaArr = explode('.', $media); //first index is file name and second index file type
        $newMediaName = moveWithUniqueName($files['media' . $mediaSuffix]['tmp_name'][$key],
                                         $mediaArr[0],
                                         $mediaArr[1]);

        if (!$newMediaName) {
            echo 'something went wrong';
        } else if (insertMedia($newMediaName, $typeMedia, $idPost) == false) {
            EDatabase::rollBack();
            return;
        }
    }
}

/**
 * Save media with unique name into folder
 *
 * @param String $from
 * @param String $basename
 * @param String $ext
 * @return $newMediaName
 */
function moveWithUniqueName($from, $basename, $ext) {
  // TODO: what if the name clashes? Should retry with another $rand
  $rand = rand(1000000, 9999999);
  $newMediaName = $basename . $rand . '.' . $ext;
  $toDir="./uploads/".$newMediaName;
  if (move_uploaded_file($from, $toDir)) {
    return $newMediaName;
  } else {
    return NULL;
  }
}

function updateComment($id, $comment)
{
    $sql= "UPDATE posts SET commentaire = '$comment' WHERE IDPosts = $id;";

    $stmt = EDatabase::prepare($sql);
    $stmt->execute();
    return;
}

function clearPostForm(){
  unset($_SESSION["post_id"]);
}

/**
 * Function to insert a post
 *
 * @param string $commentaire
 * @return lastInsertID
 */
function insertPost($commentaire, $postType)
{
    $sql = " INSERT INTO posts (commentaire, datePost, postType) VALUES (:c, :ts, :pt)";

    $sth = EDatabase::prepare($sql);
    try {
        $sth->execute(array(
            ':c' => $commentaire,
            ':ts' => date("Y-m-d"),
            ':pt' => $postType
        ));
    } catch (PDOException $e) {
        echo 'Problème de lecture de la base de données: ' . $e->getMessage();
        EDatabase::rollBack();
        return false;
    }
    // On retourne l'id du record inséré
    return EDatabase::lastInsertId();
}

/**
 * Function for debuging
 *
 * @param any $data
 * @return void
 */
function debug($data)
{
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
}

/**
 * Show alert with message in $_SESSION["message"]
 *
 * @return void
 */
function flashMessage()
{
    if (isset($_SESSION["message"])) {
        $HTMLmsg = <<<MSG
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      <strong>Holy guacamole!</strong> {$_SESSION["message"]}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
MSG;
        unset($_SESSION["message"]);
        return $HTMLmsg;
    } else {
        return false;
    }
}

// https://www.php.net/manual/fr/features.file-upload.errors.php

/**
 * function to show upload errors
 *
 * @param int $code
 * @return $message
 */
function codeToMessage($code)
{
    switch ($code) {
        case UPLOAD_ERR_INI_SIZE:
        case 1:
            $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
            break;
        case UPLOAD_ERR_FORM_SIZE:
        case 2:
            $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
            break;
        case UPLOAD_ERR_PARTIAL:
        case 3:
            $message = "The uploaded file was only partially uploaded";
            break;
        case UPLOAD_ERR_NO_FILE:
        case 4:
            $message = "No file was uploaded";
            break;
        case UPLOAD_ERR_NO_TMP_DIR:
        case 6:
            $message = "Missing a temporary folder";
            break;
        case UPLOAD_ERR_CANT_WRITE:
        case 7:
            $message = "Failed to write file to disk";
            break;
        case UPLOAD_ERR_EXTENSION:
        case 8:
            $message = "File upload stopped by extension";
            break;

        default:
            $message = "Unknown upload error";
            break;
    }
    return $message;
}



/**
 * Function to show different type of medias
 *
 * @param int $id
 * @param String $type
 * @param String $path
 * @return void
 */
function showMedia($id, $MIMEtype, $path)
{
    $media = "";
    //Check if media have type
    if ($MIMEtype != "") {
        list($media, $type) = explode("/", $MIMEtype);
    }

    if ($media != "") {
        ?>
      <div class="text-center">
      <?php

      //Check if media is a video
      if ($media == "video") {
          ?>
        <div class="card text-center">
          <iframe class="embed-responsive" src="uploads/<?=$path?>" type="<?=$MIMEtype?>" style="border:none" allowfullscreen></iframe>
        </div>
        <?php
      }

        //Check if media is an audio
        if ($media == "audio") {
            ?>
        <audio controls src="uploads/<?=$path?>" style="width:100%"> Your browser does not support the <code>audio</code> element. </audio>
        <?php
        }

        //Check if media is an image
        if ($media == "image") {
            ?>
      <div class="card text-center">
        <button type="button" class="btn" data-toggle="modal" data-target="#img-<?= $id ?>">
          <img class="card-img-top img-fluid" src="uploads/<?=$path?>" alt="oups">
        </button>
      </div>
      <?php
        } ?>

    </div>
    <div class="modal fade" id="img-<?=$id?>" tabindex="-1" role="dialog" aria-labelledby="imgLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <?php
            //Check if media is an image
            if ($media == "image") {
                ?>
              <img class="card-img-top img-fluid" src="uploads/<?=$path?>" alt="oups">
              <?php
            } ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <?php
    }
}

/**
 * Delete a specified post
 *
 * @param int $postId
 * @return void
 */
function deletePost($postId)
{
    $sql= "DELETE FROM posts WHERE posts.IDPosts = $postId;
           DELETE FROM media WHERE media.posts_IDPosts = $postId;";
    $stmt = EDatabase::prepare($sql);
    $stmt->execute();
    header('Location: index.php');
    exit;
}

/**
 * Delete a post media
 *
 * @param int $mediaId
 * @return void
 */
function deleteMedia($mediaId)
{
    $sql= "DELETE FROM media WHERE media.idmedia = $mediaId;";
    $stmt = EDatabase::prepare($sql);
    $stmt->execute();
    header('Location: post.php');
    exit;
}

/**
 * Show all media from an existing post
 *
 * @param int $postId
 * @return void
 */
function showMediaToEdit($postId)
{
    $tmp = getPostMediaWhereId($postId);

    echo "<ul class='list-group list-group-flush'>";
    foreach ($tmp as $media) {
        ?>

        <li class='list-group-item'>
          <?= $media->nomMedia ?>
          <button type='button' name='deleteMedia_button' class='close' data-dismiss='modal' aria-label='Close'>
          <a href="?media_id=<?= $media->idmedia ?>">
            <span aria-hidden='true'>&times;</span>
          </a>
          </button>
        </li>

      <?php
    }
    echo "</ul>";
}

/**
 * Get the comment of an existing post
 *
 * @param int $postId
 * @return String $valueComment
 */
function showCommentToEdit($postId)
{
    try {
        $data = getCommentWhereId($postId);
        $valueComment = $data->commentaire;
    } catch (\Exception $e) {
        $valueComment = "";
    }
    echo $valueComment;
}

/**
 * Get the type of an existing post specified by an id
 *
 * @param int $id
 * @return $postType
 */
function getPostTypeWhereId($id)
{
    $sql= "SELECT postType from posts WHERE IDPosts = $id order by IDPosts desc";
    $stmt = EDatabase::prepare($sql);
    $stmt->execute();
    $postType = $stmt->fetch(PDO::FETCH_ASSOC);
    return $postType;
}

/**
 * Generic function to create radio button
 *
 * @param int $id_radio
 * @param String $value_radio
 * @param String $accept_file
 * @param String $name_file
 * @return void
 */
function printRadio($id_radio, $value_radio, $accept_file, $name_file) { ?>
  <div class="custom-control custom-radio">
    <input checked type="radio" id="<?=$id_radio?>" name="mediaType" class="custom-control-input" value="<?=$value_radio?>">
    <label class="custom-control-label" for="<?=$id_radio?>"><?=$value_radio?></label>
    <div class="form-group">
      <input type="file" multiple accept="<?=$accept_file?>" class="form-control-file" name="<?=$name_file?>[]" id="<?=$name_file?>">
    </div>
  </div>
<?php };

/**
 * function to show the right radio button into post
 *
 * @return void
 */
function radioBtn()
{
  $form = array();
  $form["Image"] = function() { printRadio("mediaTypeImg", "Image", "image/*", "mediaI"); };
  $form["Video"] = function() { printRadio("mediaTypeVideo", "Video", "video/*", "mediaV"); };
  $form["Audio"] = function() { printRadio("mediaTypeAudio", "Audio", "audio/*", "mediaA"); };

  if (isset($_SESSION["post_id"])) {
    $typeMedia = getPostTypeWhereId($_SESSION["post_id"]);
    if ($typeMedia["postType"]) {
      call_user_func($form[$typeMedia["postType"]]);
    }
  } else {
    foreach ($form as $type => $func) {
      call_user_func($func);
    }

 }
}

function print_posts($date = NULL){
  $tmp = getPostMedia($date);
  if (!count($tmp)) {
    if ($date) {
      ?>
      <h1>Aucun resultat !!</h1>
      <?php
    }else {
      ?>
      <h1>Aucune donnée en base !!</h1>
      <?php
    }
  }
  foreach ($tmp as $post) {
      ?>
    <div class="card center mt-4">
      <div class="card-header">
        <?= $post["postType"] ?>
        <br>
        <?= $post["datePost"] ?>
        <form name="delete_post" action="" class="delete_post" method="post">
          <input type="hidden" name="post_id" value="<?= $post['id'] ?>">

          <button type="submit" name="edit_button" class="btn btn-default btn-lg" aria-label="Edit">
            <i class="fa fa-edit"></i>
          </button>

          <button type="submit" name="delete_button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </form>
      </div>
      <div class="card-body">
        <p class="card-text">
          <?= $post["commentaire"] ?>
        </p>

        <?php
        if (!is_null($post["medias"][0][0])): ?>

        <div class="card-columns" style="column-count: 5">
          <?php foreach ($post["medias"] as $media):
            showMedia($media[0], $media[1], $media[2]); ?>

          <?php endforeach; ?>
        </div>
      <?php endif; ?>
      </div>
    </div>
    <?php
  }

}

/**
 * Function to set the footer with all scrips
 *
 */
function printFooterScripts()
{
    ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script type="text/javascript">
    $(function() {
      $("input#searchByDate").change(async function(event){
        var date = $(event.target).val();
        var response = await fetch('searchAjax.php?date='+date);
        var text = await response.text()
        $("div#postsContainer").html(text);
      })
    })

    </script>
    <?php
}
