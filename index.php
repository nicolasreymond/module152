<?php
require_once './function.inc.php';

clearPostForm();

if (isset($_POST["delete_button"])){
  deletePost($_POST["post_id"]);
}

if (isset($_POST["edit_button"])){
  $_SESSION["post_id"] = $_POST["post_id"];
  header('Location: post.php');
}

?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" crossorigin="anonymous">

  <title>HOME!</title>
</head>

<body>
  <!-- NavBar -->
  <?php include 'navbar.php'; ?>
  <div class="container">
    <!-- Welcome -->
    <blockquote class="blockquote text-center">
      <p class="mb-0">Bienvenue !!</p>
      <footer class="blockquote-footer">Someone famous in <cite title="Source Title">IDA-D3A</cite></footer>
    </blockquote>
    <!-- Image -->
    <img src="images/plane.png" class="rounded mx-auto d-block" alt="cube" style="width:75%">

    <div id="postsContainer" class="container">
      <?php print_posts(); ?>
    </div>

  </div>
  <?php printFooterScripts(); ?>
</body>

</html>
