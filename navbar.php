<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">

  <a class="navbar-brand" href="index.php"><img src="images/cube.png" class="rounded float-left" alt="cube" style="width:60px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
  <div class="navbar-nav">
    <?php
      $isPostActive  = $_SERVER['PHP_SELF'] == "/post.php";
      $isIndexActive = $_SERVER['PHP_SELF'] == "/index.php";
      ?>
      <a class="nav-item nav-link <?= $isIndexActive ? "active" : "" ?>" href="index.php">Home      <span class="sr-only"></span></a>
      <a class="nav-item nav-link <?= $isPostActive  ? "active" : "" ?>" href="post.php?new=1">Post <span class="sr-only"></span></a>
      <input type="date" id="searchByDate" name="" value="">
  </div>
</div>
</nav>
